GCC = g++
OPTIONS = -Wall -Wextra
NAME = asm_piguet
MAIN = piguet_soc_assembler

${NAME}: ${MAIN}.cpp ${MAIN}.hpp
	${GCC} -o build/${NAME} ${MAIN}.cpp ${OPTIONS}

clean:
	rm ${NAME}.exe

check:
	cppcheck --enable=all --language=c++ ${MAIN}.cpp
