----------------------------------------------------------------------------------
-- Company: HEIA-FR
-- Engineer: 
-- 
-- Create Date:    08:46:40 26/02/2024 
-- Design Name: 
-- Module Name:    prog_mem - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.miniproc_pkg.all;

entity prog_mem is
	port (
        clk_i : in std_logic;
        adr_i : in std_logic_vector(cPROGR_ADR_SIZE-1 downto 0);
        data_i : in std_logic_vector(cINSTR_SIZE-1 downto 0);
        data_o : out std_logic_vector(cINSTR_SIZE-1 downto 0);
        ce_i : in std_logic;
        rw_i : in std_logic
	);
end prog_mem;

architecture Behavioral of prog_mem is

	type prog_mem_type is array(1023 downto 0) of std_logic_vector(cINSTR_SIZE-1 downto 0);
	--program memory
	signal prog_mem_s : prog_mem_type := ( --    short program:
		-- CODE HERE
		0   => "0000000000000000",	--         LOAD R0,DM[0]
		1   => "0000100000000001",	--         LOAD R1,DM[1]
		2   => "0111000010000000",	--         MULT R2,R0,R1
		3   => "0011000000000010",	--         STORE R2,DM[2]
		4   => "1011100000000000",	--         EQL0 R3
		5   => "1100000000000010",	--         JMPC EMPTY
		6   => "1110000000010000",	--         JMP ERROR
		7   => "0101011000000000",	-- EMPTY   CPY R2,R3
		8   => "0001100000000100",	--         LOAD R3, DM[4]
		9   => "1001010010000000",	-- LOOP    ADD R2,R2,R1
		10  => "1000000110000000",	--         ADD R0,R0,R3
		11  => "1010000000000000",	--         EQL0 R0
		12  => "1110000000000010",	--         JMPC COMPARE
		13  => "1100011111111100",	--         JMP LOOP
		14  => "0011000000000011",	-- COMPARE STORE R2,DM[3]
		15  => "0000100000000010",	--         LOAD R1,DM[2]
		16  => "0110101110000000",	--         MULT R1,R1,R3
		17  => "1000001100000000",	--         ADD R0,R1,R2
		18  => "1010000000000000",	--         EQL0 R0
		19  => "1110000000000010",	--         JMPC END
		20  => "1100000000000010",	--         JMP ERROR
		21  => "1100000000000000",	-- END     JMP END
		22  => "1100000000000000",	-- ERROR   JMP ERROR
		others => "0000000000000000"
	);
	
begin
	
    process (clk_i)
    begin
        if falling_edge(clk_i) then
            if ce_i='1' then
                if rw_i=cWRITE then
                    prog_mem_s(to_integer(unsigned(adr_i))) <= data_i;
                else
                   data_o <= prog_mem_s(to_integer(unsigned(adr_i)));
               end if;
            end if;
        end if;
    end process;

end Behavioral;

