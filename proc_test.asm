; Beginning of program
LOAD R0,0
LOAD R1,1
EQL0 R2
JMPC MULT
JMP ERROR
MULT:
MULT R2,R0,R1
STORE R2,2
EQL0 R3
JMPC EMPTY
JMP ERROR
EMPTY:
CPY R2,R3
LOAD R3,4
LOOP:
ADD R2,R2,R1
ADD R0,R0,R3
EQL0 R0
JMPC COMPARE
JMP LOOP
COMPARE:
STORE R2,3
LOAD R1,2
MULT R1,R1,R3
ADD R0,R1,R2
EQL0 R0
JMPC END
JMP ERROR
END:
JMP END
ERROR:
JMP ERROR
