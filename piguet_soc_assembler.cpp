/*****************************************************************//**
 * \file   piguet_soc_assembler.cpp
 * \brief  
 * 
 * \author golde
 * \date   March 2024
 *********************************************************************/

#include "piguet_soc_assembler.hpp"

int main(int argc, char* argv[]) {
	string output_name = "a.bin";
	string input_name;
	string vhdl_name;
	//string instr_set_name;

	opcode_s opcodes[8] = {
		{"LOAD" , 0u << OPCODE_POSE},
		{"STORE", 1u << OPCODE_POSE},
		{"CPY"  , 2u << OPCODE_POSE},
		{"MULT" , 3u << OPCODE_POSE},
		{"ADD"  , 4u << OPCODE_POSE},
		{"EQL0" , 5u << OPCODE_POSE},
		{"JMP"  , 6u << OPCODE_POSE},
		{"JMPC" , 7u << OPCODE_POSE}
	};

	label_s labels[MAX_LABEL];

	if (argc < 2) {
		cerr << "Argument number not right, require at leat 1." << endl;
		help();
		exit(0);
	}

	for (int i = 1; i < argc; i++) {
		clog << "Argument: " << i << endl;
		if (string(argv[i]) == "-o") {
			i++;
			output_name = argv[i];
		}
		else if (string(argv[i]) == "-v") {
			i++;
			vhdl_name = argv[i];
		}
		/*else if (string(argv[i]) == "-i") {
			instr_set_name = argv[i + 1];
		}*/
		else if (string(argv[i]) == "-h") {
			help();
			exit(0);
		}
		else {
			if (input_name.empty()) {
				input_name = argv[i];
			}
			else {
				cerr << "Input file name already given: " << argv[i] << endl;
				exit(-1);
			}
		}
	}
	clog << "input_name : " << input_name << endl;
	clog << "output_name : " << output_name << endl;
	//clog << "instr_set_name : " << instr_set_name << endl;

	clog << "Opening programm file." << endl;
	ifstream input_file(input_name);
	if (input_file.fail()) {
		cerr << "Error, cannot open input file." << endl;
		exit(-1);
	}

	clog << "Opening binary file." << endl;
	ofstream output_file(output_name);
	if (output_file.fail()) {
		cerr << "Error, cannot open output file." << endl;
		exit(-1);
	}
	
	clog << "Assembly out" << endl;

	get_label_location(labels, input_file);

	string line;
	unsigned int line_number = 1;
	
	clog << "---- Begin code ----" << endl;
	do  {
		getline(input_file, line);
		//clog << line << endl;
		if (line.empty() || line.front() == ';') {
			continue;
		}
		if (line.back() == ':') {
			continue;
		}

		cout << "--- Next instruction ---" << endl;
		cout << "-- line " << line_number << " --" << endl;

		size_t opcode_pos = line.find(' '); // Split at space, for instruction
		string curr_code = line.substr(0, opcode_pos); // Get operation code name
		unsigned short command = 0u;
		if (curr_code == opcodes[0].code) {
			clog << "Load instruction" << endl;
			command |= opcodes[0].bin; // Get opcode binary

			size_t register_pos = line.find(','); // Get comma position 
			string regist = line.substr(opcode_pos + 1, 2); // get register name
			clog << "Register: " << regist << endl;
			command |= get_register(regist, 1); // Get register binary

			size_t addr_pos = register_pos + 1;
			string addr = line.substr(addr_pos, string::npos); // get register name
			clog << "Address: " << addr << endl;
			command |= get_memory(addr);
		}
		else if (curr_code == opcodes[1].code) {
			clog << "Store instruction" << endl;
			command |= opcodes[0].bin; // Get opcode binary

			size_t register_pos = line.find(','); // Get comma position 
			string regist = line.substr(opcode_pos + 1, 2); // get register name
			clog << "Register: " << regist << endl;
			command |= get_register(regist, 1); // Get register binary
			
			size_t addr_pos = register_pos + 1;
			string addr = line.substr(addr_pos, string::npos); // get register name
			clog << "Address: " << addr << endl;
			command |= get_memory(addr);
		}
		else if (curr_code == opcodes[2].code) {
			clog << "Copy instruction" << endl;
			command |= opcodes[2].bin;

			size_t register_pos = line.find(','); // Get comma position 
			string regist_1 = line.substr(opcode_pos + 1, REGISTER_LENGTH); // get register name
			clog << "Register 1: " << regist_1 << endl;
			command |= get_register(regist_1, 1); // Get register binary

			string regist_2 = line.substr(register_pos + 1, string::npos); // get register name
			clog << "Register 2: " << regist_2 << endl;
			command |= get_register(regist_2, 2); // Get register binary
		}
		else if (curr_code == opcodes[3].code) {
			clog << "Multiplication instruction" << endl;
			command |= opcodes[3].bin;

			size_t register_pos = line.find(','); // Get comma position 
			string regist_1 = line.substr(opcode_pos + 1, REGISTER_LENGTH); // get register name
			clog << "Register 1: " << regist_1 << endl;
			command |= get_register(regist_1, 1); // Get register binary

			register_pos = line.find(',', register_pos) + 1; // Get comma position 
			string regist_2 = line.substr(register_pos , REGISTER_LENGTH); // get register name
			clog << "Register 2: " << regist_2 << endl;
			command |= get_register(regist_2, 2); // Get register binary

			register_pos++;
			string regist_3 = line.substr(register_pos + REGISTER_LENGTH, string::npos); // get register name
			clog << "Register 3: " << regist_3 << endl;
			command |= get_register(regist_3, 3); // Get register binary
		}
		else if (curr_code == opcodes[4].code) {
			clog << "Addition instruction" << endl;
			command |= opcodes[4].bin;

			size_t register_pos = line.find(','); // Get comma position 
			string regist_1 = line.substr(opcode_pos + 1, REGISTER_LENGTH); // get register name
			clog << "Register 1: " << regist_1 << endl;
			command |= get_register(regist_1, 1); // Get register binary

			register_pos = line.find(',', register_pos) + 1; // Get comma position 
			string regist_2 = line.substr(register_pos, REGISTER_LENGTH); // get register name
			clog << "Register 2: " << regist_2 << endl;
			command |= get_register(regist_2, 2); // Get register binary

			register_pos++;
			string regist_3 = line.substr(register_pos + REGISTER_LENGTH, string::npos); // get register name
			clog << "Register 3: " << regist_3 << endl;
			command |= get_register(regist_3, 3); // Get register binary
		}
		else if (curr_code == opcodes[5].code) {
			clog << "Equal to zero instruction" << endl;
			command |= opcodes[5].bin; // Get opcode binary

			string regist = line.substr(opcode_pos + 1, 2); // get register name
			clog << "Register: " << regist << endl;
			command |= get_register(regist, 1); // Get register binary
		}
		else if (curr_code == opcodes[6].code) {
			clog << "Jump instruction" << endl;
			command |= opcodes[6].bin;

			string label_name = line.substr(opcode_pos + 1, string::npos);
			clog << "Label name: " << label_name << endl;
			short label_offset = get_label_offset(labels, label_name, line_number);
			clog << "Label offset: " << label_offset << endl;
			command |= label_offset;
		}
		else if (curr_code == opcodes[7].code) {
			clog << "Jump condition instruction" << endl;
			command |= opcodes[7].bin;

			string label_name = line.substr(opcode_pos + 1, string::npos);
			clog << "Label name: " << label_name << endl;
			short label_offset = get_label_offset(labels, label_name, line_number);
			clog << "Label offset: " << label_offset << endl;
			command |= label_offset;
		}
		else {
			cerr << "Error instruction not found: " << curr_code << endl;
			//exit(-1);
		}
		clog << "Binary: " << hex << command << endl;
		output_file.write((const char*)&command, 2);

		line_number++;
	} while (!input_file.eof());
	clog << "End of programm" << endl;

	output_file.close();
	input_file.close();

	return 0;
}

void help(void) {
	cout << "./assembler_piguet <assembly_file.asm> <options>" << endl 
		<< "\t-o\tOutput filename" << endl 
		<< "\t-i\tInstruction set file" << endl
		<< "\t-v\tVHDL template file" << endl
		<< "\t-h\tShow this help" << endl;
}

unsigned short get_register(string& reg, unsigned char pos) {
	unsigned short reg_bin = 0x03ffu;
	int shifter;
	switch(pos){
	case 1:
		shifter = REGISTER1_POSE;
		break;
	case 2:
		shifter = REGISTER2_POSE;
		break;
	case 3:
		shifter = REGISTER3_POSE;
		break;
	default:
		shifter = REGISTER1_POSE;
		break;
	}

	if (reg == "R0") {
		reg_bin = 0x00u;
	}
	else if (reg == "R1") {
		reg_bin = 0x01u;
	}
	else if (reg == "R2") {
		reg_bin = 0x02u;
	}
	else if (reg == "R3") {
		reg_bin = 0x03u;
	}
	return (reg_bin << shifter);
}

unsigned short get_memory(string& mem) {
	clog << "get_memory: mem: " << mem << endl;
	return (unsigned short)stoi(mem);
}

void add_label(label_s label_list[], string label_name, unsigned int line_number) {
	for (int i = 0; i < MAX_LABEL; i++) {
		if (label_list[i].name.empty()) {
			label_list[i].name = label_name;
			label_list[i].position = line_number;
			return;
		}
	}
	cerr << "Label not found: " << label_name << endl;
	exit(-1);
}

short get_label_offset(label_s label_list[], string label_name, unsigned int line_number) {
	for (int i = 0; i < MAX_LABEL; i++) {
		if (label_list[i].name == label_name) {
			clog << "Label position: " << dec << label_list[i].position << endl;
			return (label_list[i].position - line_number) & 0x07ff;
		}
	}
	cerr << "Label not found: " << label_name << endl;
	exit(-1);
	return 0;
}

void get_label_location(label_s label_list[], ifstream &input_file) {
	clog << "Getting label location" << endl;
	string line;
	unsigned int line_number = 1;
	do
	{
		getline(input_file, line);
		//clog << line << endl;
		if (line.empty() || line.front() == ';') {
			clog << line << endl;
			continue;
		}
		if (line.back() == ':') {
			size_t label_length = line.length() - 1;
			add_label(label_list, line.substr(0, label_length), line_number);
			clog << "-- Location: " << line << "L" << line_number << endl;
			continue;
		}
		line_number++;
	} while (!input_file.eof());
	input_file.clear();
	input_file.seekg(0);
}
