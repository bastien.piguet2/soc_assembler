#pragma once

#include <iostream>
#include <fstream>
#include <cstring>
#include <string>

#define MAX_PATH_LENGTH 1024

#define COMMAND_LENGTH 16
#define OPCODE_LENGTH 3
#define OPCODE_POSE (COMMAND_LENGTH - OPCODE_LENGTH)
#define REGISTER_LENGTH 2
#define REGISTER1_POSE (OPCODE_POSE - REGISTER_LENGTH)
#define REGISTER2_POSE (REGISTER1_POSE - REGISTER_LENGTH)
#define REGISTER3_POSE (REGISTER2_POSE - REGISTER_LENGTH)
#define ADDRESS_LENGTH 11
#define ADDRESS_POSE (REGISTER_POSE - ADDRESS_LENGTH)
#define MAX_LABEL 100

using namespace std;

typedef struct  {
	char code[8];
	unsigned short bin = 0;
}opcode_s;

typedef struct {
	string name;
	unsigned int position;
}label_s;

void help(void);
unsigned short get_register(string& reg, unsigned char pos);
unsigned short get_memory(string& mem);
void add_label(label_s label_list[], string label_name, unsigned int line_number);
short get_label_offset(label_s label_list[], string label_name, unsigned int line_number);
void get_label_location(label_s label_list[], ifstream &input_file);